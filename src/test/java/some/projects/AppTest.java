package some.projects;

//import junit.framework.Test;
//import junit.framework.TestCase;
//import junit.framework.TestSuite;
//import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.io.BufferedReader;
import static org.junit.Assert.*;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;

import java.io.IOException;

/**
 * Unit test for simple App.
 */
@RunWith(value = Parameterized.class)
public class AppTest 
{
    
    static Collection<String> dataArray = null;
    
    private String str;
    private String operand1;
    private String operand2;
    private String operation;
    private String result;
    

    public AppTest(String operand1, String operand2, String operation, String result){

         this.operand1 = operand1;
         this.operand2 = operand2;
         this.operation = operation;
         this.result = result;
     }
    
    public static Collection<String[]> ReadData (String cvsfileName) throws IOException {
        
        String s;
        String splitSymbol = ";";
        List<String[]> dataArray = new ArrayList<String[]>();
        BufferedReader buffer = new BufferedReader(new FileReader(cvsfileName));
        try {
            while((s=buffer.readLine())!=null) {

                String fields[] = s.split(splitSymbol);
 
                dataArray.add(fields);
            }
        }
        catch(IOException ex){
            
            System.out.println(ex.getMessage());
        }
        
        buffer.close();
        
        return dataArray;
    }
    
    
    @Parameters
    public static Collection<String[]> data() throws IOException {
     
        return ReadData("src/main/resources/table1.csv");

      }

    @Title("�������� ��������������� ���������")
    @Test
    public void testMathOperators() {
       
        testMathResult(this.operand1, this.operand2, this.operation, this.result);
    }

    @Step("��������� �������� ��������������� ���������:  {0} {2} {1} = {3}")
    public void testMathResult (String op1, String op2, String operator, String res) {
        
        assertTrue("������� ����� ��������",(operation.equals("+")) | (operation.equals("-")) | (operation.equals("/")) | (operation.equals("*")));
        
        try {

                if (operator.equals("+")){
                    assertEquals("�������� ��������� ��������",Integer.parseInt(op1)+Integer.parseInt(op2), Integer.parseInt(res));
                    System.out.println(Integer.parseInt(op1)+ " " +Integer.parseInt(op2));
                }

                if (operator.equals("-")){
                    assertEquals("�������� ��������� ��������",Integer.parseInt(op1)-Integer.parseInt(op2), Integer.parseInt(res));
                    System.out.println(Integer.parseInt(op1)+ " " +Integer.parseInt(op2));
                }

                if (operator.equals("/")){
                    assertEquals("�������� ��������� ��������",Integer.parseInt(op1)/Integer.parseInt(op2), Integer.parseInt(res));
                    System.out.println(Integer.parseInt(op1)+ " " +Integer.parseInt(op2));
                }

                if (operator.equals("*")){
                    assertEquals("�������� ��������� ��������",Integer.parseInt(op1)*Integer.parseInt(op2), Integer.parseInt(res));
                    System.out.println(Integer.parseInt(op1)+ " " +Integer.parseInt(op2));
                }
        }
        catch (ArithmeticException ex) {
            
            System.out.println(ex.getMessage());
            fail("������������ �������������� ��������");
        }
        catch (NumberFormatException ex) {
            
            System.out.println(ex.getMessage());
            fail("������������ ��������������");
         } 
    }
}
